# Neovim Configuration
My configuration files
## Plugins Installation
In order to install [Packer](https://github.com/wbthomason/packer.nvim) use command
```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```
