require('packer').startup(function(use)
	-- Packer manager itself
	use 'wbthomason/packer.nvim'
	-- colorscheme
	use({
		"Shatur/neovim-ayu",
		-- Optional; default configuration will be used if setup isn't called.
		config = function()
			require("ayu").setup({
				background = "dark",
			})
		end,
	})
	-- tmux integration
	use "christoomey/vim-tmux-navigator"
	-- Installer of LSP servers
	use { "williamboman/mason.nvim",
		"williamboman/mason-lspconfig.nvim",
	}
	-- Configuration for Neovim LSP
	use "neovim/nvim-lspconfig"
	-- Autocompletion plugin
	use 'hrsh7th/nvim-cmp'
	-- Buffer source for nvim-cmp
	use 'hrsh7th/cmp-buffer'
	-- LSP source for nvim-cmp
	use 'hrsh7th/cmp-nvim-lsp'
	-- Snippet engine
	use({
		"L3MON4D3/LuaSnip",
		-- follow latest release.
		tag = "v<CurrentMajor>.*",
		-- install jsregexp (optional).
		run = "make install_jsregexp"
	})
	-- Snippet engine source for nvim-cmp
	use { 'saadparwaiz1/cmp_luasnip' }
	-- Paths of files and folders as source for nvim-cmp
	use 'hrsh7th/cmp-path'
	-- Plugin which adds vscode-like pictograms to LSP
	use 'onsails/lspkind.nvim'
	-- Plugin for studying Vims' ways of movement	
	use 'takac/vim-hardtime'
end)

require('ayu').colorscheme()
require('lspconf')
require('cmpconf')
