require("mason").setup()

require("mason-lspconfig").setup({
	ensure_installed = { "lua_ls", "cssls", "texlab", "bashls" }
})

local status, nvim_lsp = pcall(require, "lspconfig")
if (not status) then return end

local protocol = require('vim.lsp.protocol')

local on_attach = function(client, bufnr)
	-- formating
	if client.server_capabilities.documentFormattingProvider then
		vim.api.nvim_create_autocmd("BufWritePre", {
			group = vim.api.nvim_create_augroup("Format", { clear = true }),
			buffer = bufnr,
			callback = function() vim.lsp.buf.format() end
		})
	end
end

-- Completion using nvim_cmp with LSP source
local capabilities = require('cmp_nvim_lsp').default_capabilities()
local capabilities_vscode = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true


-- SERVER CONFIGURATION

-- Lua
nvim_lsp.lua_ls.setup {
	on_attach = on_attach,
	settings = {
		Lua = {
			diagnostics = {
				globals = { 'vim' },
			},
			workspace = {
				library = vim.api.nvim_get_runtime_file("", true),
				checkThirdParty = false
			},
			telemetry = {
				enable = false,
			},
		},
	},
	capabilities = capabilities
}

-- Tex
nvim_lsp.texlab.setup {
	on_attach = on_attach,
	capabilities = capabilities
}

-- CSS
nvim_lsp.cssls.setup {
	on_attach = on_attach,
	capabilities = capabilities_vscode
}

-- Bash
nvim_lsp.bashls.setup {
	on_attach = on_attach,
	capabilities = capabilities
}
