vim.opt.nu = true
vim.opt.relativenumber = true

vim.o.termguicolors = true
vim.o.background = "dark"

require('plugins')
